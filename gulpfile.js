var gulp = require('gulp');
var gutil = require('gulp-util');
var bower = require('bower');
var concat = require('gulp-concat');
var sass = require('gulp-sass');
var minifyCss = require('gulp-minify-css');
var rename = require('gulp-rename');
var uglify = require('gulp-uglify');
var sh = require('shelljs');

var paths = {
  sass: ['./scss/*.scss'],
  sass_watch: ['./scss/**/*.scss'],
  angular: [
    './www/js/app.js',
    './www/js/services/**/*.js',
    './www/js/controllers/**/*.js',
    './www/js/directives/**/*.js',
    './www/js/factories/**/*.js'
  ]
};

gulp.task('default', ['sass', 'angular']);

gulp.task('sass', function (done) {
  gulp.src(paths.sass)
    .pipe(sass({
      errLogToConsole: true
    }))
    .pipe(gulp.dest('./www/css/'))
    .pipe(minifyCss({
      keepSpecialComments: 0
    }))
    .pipe(rename({extname: '.min.css'}))
    .pipe(gulp.dest('./www/css/'))
    .on('end', done);
});

gulp.task('angular', function (done) {
  return gulp.src(paths.angular)
    .pipe(concat('app.min.js')) //the name of the resulting file
    .pipe(uglify({mangle: false}))
    .pipe(gulp.dest('./www/js/'))
});

gulp.task('watch', function () {
  gulp.watch(paths.sass_watch, ['sass']);
  gulp.watch(paths.angular, ['angular']);
});

gulp.task('install', ['git-check'], function () {
  return bower.commands.install()
    .on('log', function (data) {
      gutil.log('bower', gutil.colors.cyan(data.id), data.message);
    });
});

gulp.task('git-check', function (done) {
  if (!sh.which('git')) {
    console.log(
      '  ' + gutil.colors.red('Git is not installed.'),
      '\n  Git, the version control system, is required to download Ionic.',
      '\n  Download git here:', gutil.colors.cyan('http://git-scm.com/downloads') + '.',
      '\n  Once git is installed, run \'' + gutil.colors.cyan('gulp install') + '\' again.'
    );
    process.exit(1);
  }
  done();
});

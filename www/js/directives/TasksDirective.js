/**
 * Tasks container directive
 */
direcModule.factory('todoTasksContainer', function () {

  return {
    restrict: 'E',
    templateUrl: 'templates/todo-task-container.html'
  };

});

/**
 * task item directive
 */
direcModule.factory('todoTaskItem', function () {

  return {
    restrict: 'E',
    templateUrl: 'js/directives/templates/todo-task-item.html'
  };

});

/**
 * Lists directives
 */
direcModule.factory('todoList', function () {

  return {
    restrict: 'E',
    templateUrl: 'templates/todo-list.html'
  };

});

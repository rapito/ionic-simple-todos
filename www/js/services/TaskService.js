TaskService = {
  enabled: true,
  SERVICE_URI: "http://ionic-todos-api.meteor.com",
  SERVICE_ENPOINT: '/api/lists/',

  getURI: function (suffix) {
    if (!this.enabled) return;
    suffix = suffix === 'undefined' || !suffix ? '' : suffix;
    return this.SERVICE_URI + this.SERVICE_ENPOINT + suffix;
  },

  getLists: function (http, cb) {
    if (!this.enabled) return;
    http.get(this.getURI()).success(cb).error(function (err) {
      console.log("Couldn't getLists: ", err);
    });
  },

  saveLists: function (http, lists) {
    if (!this.enabled) return;
    http.post(this.getURI('save'), lists);
  }


};

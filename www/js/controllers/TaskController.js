/**
 * Handles listing of tasks and its beaviour
 */
ctrlModule.controller('TaskController', function ($rootScope, $scope, $http, $ionicPopup, $ionicSideMenuDelegate, ListService, LocalLists) {

  $scope.list = ListService.currentList;
  $scope.pendingTasks = 0;

  $scope.toggleLeft = function () {
    $ionicSideMenuDelegate.toggleLeft();
  };

  /**
   * Creates new task object and persists list
   */
  $scope.createTask = function (oTask) {

    var task = angular.copy(oTask);
    task.done = false;

    $scope.list.tasks.unshift(task);
    updatePendingTasks();
    ListService.saveList($rootScope);

    // clearform
    oTask.task = "";
    if ($scope.formAddTask)
      $scope.formAddTask.$setPristine();
  };

  /**
   * Changes 'done' state of task
   * updates pending count
   */
  $scope.toggleTask = function (task) {
    task.done = !task.done;
    updatePendingTasks();
    ListService.saveList($rootScope);
  };

  /**
   * Removes task from our list.
   * Updates pending count
   */
  $scope.deleteTask = function (task) {
    $scope.list.tasks = $scope.list.tasks.filter(function (t) {
      return t.task !== task.task;
    });
    updatePendingTasks();
    ListService.saveList($rootScope);
  };

  /**
   * Emits broadcast to delete list
   */
  $scope.deleteList = function (list) {
    var confirmPopup = $ionicPopup.confirm({
      title: 'Delete list ' + $scope.list.name,
      template: 'Are you sure you want to delete this List?'
    });
    confirmPopup.then(function (res) {
      if (res) {
        ListService.deleteList(list, $rootScope)
      }
    });
  };

  /**
   * Handles change of a selected list.
   * - Gets the new list
   * - Counts pending tasks
   */
  $rootScope.$on(ListService.events.ON_SELECTED_LIST, function (evt, list) {
    console.log("Received broadcast: ", list);
    $scope.list = list;
    updatePendingTasks();
  });

  var updatePendingTasks = function () {
    $scope.pendingTasks = countPendingTasks();
  };

  /**
   * Filters done tasks and get the count.
   */
  var countPendingTasks = function () {
    if ($scope.list === null || $scope.list.tasks === null) return;

    var pending = 0;
    angular.forEach($scope.list.tasks, function (t) {
      pending += !t.done ? 1 : 0;
    });
    return pending;
  };

  var init = function () {
    countPendingTasks();
  };

  init();
});


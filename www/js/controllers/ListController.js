/**
 * Handles side menu lists
 */
ctrlModule.controller('ListController', function ($rootScope, $scope, $http, $ionicSideMenuDelegate, RemoteLists, LocalLists, ListService) {

  $scope.lists = LocalLists.all();

  var selectList = function (list, toggleMenu) {
    if (toggleMenu) {
      $ionicSideMenuDelegate.toggleLeft();
    }
    ListService.selectList(list, $rootScope);
  };

  /**
   * Creates a new list object
   * Adds it to our lists array
   * Persists it
   * Select it
   */
  $scope.createList = function (oList) {
    var list = LocalLists.newList(oList.name);
    $scope.lists.unshift(list);
    LocalLists.save($scope.lists);
    RemoteLists.save($scope.lists);

    selectList(list);

    // clear form
    oList.name = "";
  };

  /**
   * Broadcasts a list selection
   */
  $scope.selectList = selectList;

  /**
   * Handles requests to save changes on a list.
   */
  $rootScope.$on(ListService.events.ON_LIST_CHANGED, function (evt, list) {
    console.log("Received broadcast: ", ListService.events.ON_LIST_CHANGED, list);
    LocalLists.save($scope.lists);
    RemoteLists.save($scope.lists);
  });

  /**
   * Deletes list from datasource and select a new list
   */
  $rootScope.$on(ListService.events.ON_LIST_DELETED, function (evt, list) {
    console.log("Received broadcast: ", ListService.events.ON_LIST_DELETED, list);
    var lists = [];
    angular.forEach($scope.lists, function (l) {
      if (l !== list) {
        lists.push(l);
      }
    });

    $scope.lists = lists;
    LocalLists.save(lists);
    RemoteLists.save($scope.lists);

    lists = lists.length > 0 ? lists[0] : LocalLists.first();
    selectList(lists);
  });


  var init = function () {
    RemoteLists.all(function (data) {
      data = angular.fromJson(data);
      $scope.lists = data.data;
    });
  };

  init();

});


/**
 * Handles localstorage access as a data source for persisting lists
 */
factModule.factory('LocalLists', function () {

  return {
    all: function () {
      var lists = window.localStorage['lists'];
      var empty = false;

      if (lists) {
        lists = angular.fromJson(lists);
        empty = lists.length === 0;
      }
      else {
        empty = true;
      }

      if (empty) {
        lists = [];
        lists.push(this.newList("Sample List"));
        this.save(lists);
        return this.all();
      }

      return lists;
    },
    first: function () {
      var lists = this.all();
      return lists.length > 0 ? lists[0] : null;
    },
    save: function (lists) {
      window.localStorage['lists'] = angular.toJson(lists);
    },
    newList: function (name) {
      return {
        name: name,
        tasks: []
      };
    }
  };


});

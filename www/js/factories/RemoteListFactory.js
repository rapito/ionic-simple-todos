/**
 * Handles localstorage access as a data source for persisting lists
 */
factModule.factory('RemoteLists', function ($http) {

  return {
    all: function (cb) {
      TaskService.getLists($http, cb);
    },
    first: function () {
    },
    save: function (lists) {
      TaskService.saveLists($http, lists);
    },
    newList: function (name) {
      return {
        name: name,
        tasks: []
      };
    }
  };


});
